/**
 * 
 * Copyright (C) 2017 Emmanuel DESMONTILS
 * 
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 * 
 * 
 * 
 * E-mail:
 * Emmanuel.Desmontils@univ-nantes.fr
 * 
 * 
 **/

/**
 * JFSM.java
 *
 *
 * Created: 2017-08-25
 *
 * @author Emmanuel Desmontils
 * @version 1.0
 */

import java.util.Set;
import java.util.HashSet;

import java.util.List;
import java.util.ArrayList;

import java.util.Map;
import java.util.HashMap;

import java.util.Iterator;

import JFSM.*;
import JFSM.Transducteur.*;

public class JFSM {
    public static void main(String argv []) throws JFSMException {

      Set<String> A = new HashSet<String>();      
      A.add("a");
      A.add("b");
      A.add("c");

      Set<Etat> Q = new HashSet<Etat>();
      Q.add(new Etat("1"));
      Q.add(new Etat("2"));
      Q.add(new Etat("3"));
      Q.add(new Etat("4"));
      Q.add(new Etat("5"));

      Set<Transition> mu = new HashSet<Transition>();
      mu.add(new Transition("1","a","2"));
      mu.add(new Transition("1","b","4"));
      mu.add(new Transition("2","b","3"));
      mu.add(new Transition("2","c","4"));
      mu.add(new Transition("3","a","2"));
      mu.add(new Transition("3","b","4"));
      mu.add(new Transition("4","a","5"));
      mu.add(new Transition("5","c","5"));


      Set<String> F = new HashSet<String>();
      F.add("5");
      F.add("4");
      F.add("1");
      Automate afn = new AFD(A, Q, "1", F, mu);
     
   
      List<String> l = new ArrayList<String>();
      l.add("a");
      l.add("b");
      l.add("a");
      l.add("c");
      
      /* Question 1.B
       * 
       * 
       * */
      Set<String> A1 = new HashSet<String>();      
      A1.add("GA");
      A1.add("ZO");
      A1.add("MEU");
      A1.add("BU");
      

      Set<Etat> Q1 = new HashSet<Etat>();
      Q1.add(new Etat("1"));
      Q1.add(new Etat("2"));
      Q1.add(new Etat("3"));
      Q1.add(new Etat("4"));
      Q1.add(new Etat("5"));
      Q1.add(new Etat("6"));
      Q1.add(new Etat("7"));
      Q1.add(new Etat("8"));
      Q1.add(new Etat("9"));

      Set<Transition> mu1 = new HashSet<Transition>();
      mu1.add(new Transition("1","ZO","1"));
      mu1.add(new Transition("1","GA","4"));
      mu1.add(new Transition("1","BU","5"));
      mu1.add(new Transition("2","MEU","1"));
      mu1.add(new Transition("2","BU","5"));
      mu1.add(new Transition("2","ZO","6"));
      mu1.add(new Transition("3","MEU","2"));
      mu1.add(new Transition("3","ZO","6"));
      mu1.add(new Transition("3","GA","3"));
      mu1.add(new Transition("4","GA","7"));
      mu1.add(new Transition("4","BU","8"));
      mu1.add(new Transition("4","ZO","5"));
      mu1.add(new Transition("5","BU","8"));
      mu1.add(new Transition("5","ZO","9"));
      mu1.add(new Transition("5","GA","6"));
      mu1.add(new Transition("6","ZO","9"));
      mu1.add(new Transition("6","GA","6"));
      mu1.add(new Transition("7","MEU","7"));
      mu1.add(new Transition("8","MEU","7"));
      mu1.add(new Transition("9","MEU","8"));
      mu1.add(new Transition("9","BU","9"));

      List<String> l2 = new ArrayList<String>();
      
      l2.add("MEU");
      l2.add("MEU");
      l2.add("BU");
      l2.add("ZO");
      l2.add("BU");
      l2.add("MEU");
      List<String> l3 = new ArrayList<String>();
     
      l3.add("GA");
      l3.add("BU");
      l3.add("ZO");
      l3.add("MEU");
      List<String> l4 = new ArrayList<String>();
   
      l4.add("ZO");
      l4.add("ZO");
      l4.add("GA");
      l4.add("ZO");
      l4.add("GA");
      l4.add("GA");
      l4.add("ZO");
      
      List<String> l5 = new ArrayList<String>();

      
      l5.add("BU");
      l5.add("GA");
      l5.add("ZO");
      l5.add("MEU");

      


      Set<String> F1 = new HashSet<String>();
      F1.add("7");
      F1.add("8");
      F1.add("9");
      
      Set<String> ini1 = new HashSet<String>();
      ini1.add("1");
      ini1.add("2");
      ini1.add("3");
      
      Automate afn1 = new AFN(A1, Q1, ini1, F1, mu1);
      System.out.println(afn1.run(l5));

      /* Exemple
       * 
       * 
       * */
      Set<String> A2 = new HashSet<String>();      
      A2.add("a");
      A2.add("b");
      A2.add("c");

      Set<Etat> Q2 = new HashSet<Etat>();
      Q2.add(new Etat("1"));
      Q2.add(new Etat("2"));
      Q2.add(new Etat("3"));
      Q2.add(new Etat("4"));
      Q2.add(new Etat("5"));
      Q2.add(new Etat("6"));

      Set<Transition> mu2 = new HashSet<Transition>();
      mu2.add(new Transition("1","a","1"));
      mu2.add(new Transition("1","b","2"));
      mu2.add(new Transition("1","c","3"));
      mu2.add(new Transition("2","a","5"));
      mu2.add(new Transition("3","b","2"));
      mu2.add(new Transition("3","a","4"));
      mu2.add(new Transition("4","a","5"));
      mu2.add(new Transition("6","c","4"));


      Set<String> F2 = new HashSet<String>();
      F2.add("5");
      
      Set<String> ini2 = new HashSet<String>();
      ini2.add("1");
      ini2.add("3");
      ini2.add("6");
      
      Automate afn2 = new AFN(A2, Q2, ini2, F2, mu2);
      /* Exemple 3 avec segma
       * 
       * 
       * */
      Set<String> A3 = new HashSet<String>();      
      A3.add("a");
      A3.add("b");
   


      Set<Etat> Q3 = new HashSet<Etat>();
      Q3.add(new Etat("1"));
      Q3.add(new Etat("2"));
      Q3.add(new Etat("3"));
      Q3.add(new Etat("4"));
      Q3.add(new Etat("5"));
   

      Set<Transition> mu3 = new HashSet<Transition>();
      mu3.add(new Transition("1","a","3"));
      mu3.add(new Transition("2","b","3"));
      mu3.add(new Transition("3","a","4"));
      mu3.add(new Transition("3","b","5"));
      

      Set<String> F3 = new HashSet<String>();
      F3.add("5");
      F3.add("1");
      F3.add("4");
      
      Set<String> ini3 = new HashSet<String>();
      ini3.add("1");
      ini3.add("2");
      
      Automate afn3 = new AFN(A3, Q3, ini3, F3, mu3);
     /* 
      System.out.println("----------------INITIAL--------------");
      System.out.println(afn3);
      System.out.println("----------------STANDARD--------------");
      System.out.println(afn3.standardiser());
      System.out.println("----------------NORMAL--------------");
      System.out.println(afn3.normaliser());
      System.out.println("----------------TRANSPOSE--------------");
      System.out.println(afn3.transpose());
      System.out.println("----------------ETOILE--------------");
      System.out.println(afn3.etoile());
     */
   }
}
